Polymer({
    is: 'todo-list-app',

    properties: {
        page: {
            type: String,
            reflectToAttribute: true,
            observer: '_pageChanged'
        }
    },

    listeners: {

    },

    ready: function () {

    },

    attached: function () {

    },


    // Private functions
    _routePageChanged: function (page) {
        this.page = page || 'my-view1';
    },

    _pageChanged: function (page) {
        // Load page import on demand. Show 404 page if fails
        var resolvedPageUrl = this.resolveUrl('../' + page + '/' + page + '.html');
        this.importHref(resolvedPageUrl, null, this._showPage404, true);
    },

    _showPage404: function () {
        this.page = 'my-view404';
    },

    // Observers
    observers: [
        '_routePageChanged(routeData.page)'
    ],

    // Public functions


});