Polymer({
    is: 'task-list-view',

    properties: {
       
    },

    listeners: {
       
    },

    ready: function() {
       console.log('listName: ', this._urlParams());
       this.params = this._urlParams();
    },

    attached: function() {
       
    },

    // Private functions
    _urlParams: function() {
        var queryString = location.search;
        queryString = queryString.replace('?', '');

        var querys = queryString.split('&');
        var params = {};

        for (var i = 0; i < querys.length; i++) {
            var name = querys[i].split('=')[0];
            var data = decodeURIComponent(querys[i].replace(name + '=', ''));

            if (params[name] == null) {
                params[name] = data;
            }
        }

        return params;
    },

    // Observers


    // Public functions

});